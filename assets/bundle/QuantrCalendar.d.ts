import { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
declare class QuantrCalendar extends Component<{
    style?: any;
    myCssClass?: string;
    month?: number;
    year?: number;
    hideTopBar?: boolean;
}, {}> {
    private rows;
    weekDayNames: string[];
    private currentDate;
    private hideTopBar;
    constructor(props: any);
    render(): JSX.Element;
    private changeMonth;
    private setMonth;
    private loadCalendar;
    private getWeekNums;
    private getCustomStyle;
}
export default QuantrCalendar;
