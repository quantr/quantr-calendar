'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

let config = {
	// entry: {
	// 	main: [
	// 		'./src/QuantrCalendar.jsx',
	// 		// './_devapp/css/app.css'
	// 	]
	// },
	output: {
		path: path.resolve(__dirname, 'assets', 'bundle'),
		filename: '[name].bundle.js'
	},
	resolve: {
		extensions: ['.js', '.jsx', '.ts', '.tsx']
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.(js|jsx|ts|tsx)$/,
				exclude: path.resolve(__dirname, 'node_modules'),
				use: [
					{
						loader: 'awesome-typescript-loader',
						query: {
							configFileName: './tsconfig.prod.json'
						}
					},
					// {
					// 	loader: 'babel-loader',
					// 	options: {
					// 		presets: [
					// 			'@babel/preset-env',
					// 			'@babel/preset-react',
					// 		],
					// 		plugins: [
					// 			["@babel/plugin-proposal-decorators", {
					// 				"legacy": true
					// 			}],
					// 			'@babel/plugin-syntax-dynamic-import',
					// 			['@babel/plugin-proposal-class-properties', {
					// 				"loose": true
					// 			}]
					// 		]
					// 	}
					// },
					// {
					// 	loader: 'ts-loader',
					// 	options: {
					// 		
					// 	}
					// }
				]
			},
			{
				test: /\.scss$/,
				use: [{
					loader: "style-loader",
					options: {
						sourceMap: true
					} // creates style nodes from JS strings
				},
				{
					loader: "css-loader",
					options: {
						sourceMap: true
					} // translates CSS into CommonJS
				},
				{
					loader: "sass-loader",
					options: {
						sourceMap: true
					} // compiles Sass to CSS
				}
				]
			},
			{
				test: /.(png|woff(2)?|eot|ttf|svg|gif)(\?[a-z0-9=\.]+)?$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '../css/[hash].[ext]'
					}
				}]
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader', 'postcss-loader']
			}
		]
	},
	externals: {
	},
	plugins: [
		new ExtractTextPlugin(path.join('..', 'css', 'app.css')),
		new webpack.DefinePlugin({
			'__AUTHOR__': {
				name: 'Peter',
				email: 'peter@quantr.hk'
			}
		}),
	]
};

if (process.env.NODE_ENV === 'production') {
	config.plugins.push(
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: false,
			compress: {
				sequences: true,
				conditionals: true,
				booleans: true,
				if_return: true,
				join_vars: true,
				drop_console: true
			},
			output: {
				comments: false
			},
			minimize: true
		})
	);
}

module.exports = config;