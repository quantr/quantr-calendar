'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

sass.compiler = require('node-sass');

var sassOptions = {
	outputStyle: 'compressed' // shall be compact,expanded,nested,compressed
};

gulp.task('sass', function () {
	return gulp.src('./src/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass(sassOptions).on('error', sass.logError))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./lib'));
});

gulp.task("scss", function () {
	return gulp.src(
		"./src/**/*.scss"
	).pipe(gulp.dest("./lib"));
});

// gulp.task('scss', function () {
// 	return gulp.src('./src/**/*.scss')
// 		.pipe(gulp.dest('./lib/'));
// });

// gulp.task('sass:watch', function () {
// 	gulp.watch('./sass/**/*.scss', ['sass']);
// });