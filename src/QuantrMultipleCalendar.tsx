import React, { Component } from 'react';
import logo from './logo.svg';
import styles from './QuantrMultipleCalendar.module.scss';

import { Moment } from 'moment';
var moment = require('moment');
import { number } from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faGhost, faCalendar, faCalendarAlt, faUsers, faUserMd, faMale, faClock, faMoneyBill, faPenFancy, faUserClock, faPills, faBusinessTime, faPlaneDeparture } from '@fortawesome/free-solid-svg-icons';
import * as fontawesome from '@fortawesome/free-solid-svg-icons';

import QuantrCalendar from './QuantrCalendar';

var classnames = require('classnames');

class QuantrMultipleCalendar extends Component<{ style?: any, myCssClass?: string, multiple: number[], year: number }, {}> {
	private currentDateStr: string = '';

	constructor(props: any) {
		super(props);
	}

	render() {
		let calendars: any = [];
		for (var x = 0; x < this.props.multiple.length; x++) {
			calendars.push(<td style={{ verticalAlign: 'top' }}><QuantrCalendar month={this.props.multiple[x]} year={this.props.year} hideTopBar={true} /></td>);
		}
		return (
			<div className={classnames(styles.QuantrMultipleCalendar, this.props.myCssClass)}>
				<div className="container">
					<div className="row">
						<div className="col">
							<span className="align-middle"><FontAwesomeIcon icon={fontawesome.faArrowLeft} size="1x" className={classnames("arrowLeft", styles.arrowLeft)} /></span>
						</div>
						<div className="col">
							<span className="align-middle">{this.currentDateStr}</span>
						</div>
						<div className="col">
							<span className="align-middle"><FontAwesomeIcon icon={fontawesome.faArrowRight} size="1x" className={classnames("arrowRight", styles.arrowRight)} /></span>
						</div>
					</div>
				</div>
				<table>
					<tbody>
						<tr>
							{calendars}
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}

export default QuantrMultipleCalendar;
