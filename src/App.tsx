import React, { Component } from 'react';
import logo from './image/quantrLogo.png';
import './App.css';

import 'bootstrap/dist/css/bootstrap.css';
import QuantrCalendar from './QuantrCalendar';
import QuantrMultipleCalendar from './QuantrMultipleCalendar';

class App extends Component {
	private style1 = {
		prevMonth: 'red',
		nextMonth: 'orange',
		today: '#0000ff',
		todayBackground: '#ccc'
	};
	private style2 = {
		prevMonth: 'red'
	};
	render() {
		return (
			<div className="App">
				<header className="App-header">
					<br />
					<text className="st0 st1">Quantr Calendar</text>
					Author : 
					{/* <img src={require('./image/logo.svg')} /> */}
					<br />
					<div className="container-fuild">
						<div className="row align-items-center" style={{ marginBottom: '5px' }}>
							<div className="col">
								<QuantrCalendar />
							</div>
							<div className="col" style={{ margin: 'auto' }}>
								<pre className="code">
									{`<QuantrCalendar />`}
								</pre>
							</div>
						</div>

						<div className="row align-items-center" style={{ marginBottom: '5px' }}>
							<div className="col">
								<QuantrCalendar style={this.style1} />
							</div>
							<div className="col" style={{ margin: 'auto' }}>
								<h1 className="codeH1">Styling via ReactJS</h1>
								<pre className="code">
									{`private style1 = {
	prevMonth: 'red',
	nextMonth: 'orange',
	today: '#0000ff',
	todayBackground: '#ccc'
};
<QuantrCalendar style={this.style1} />`}
								</pre>
							</div>
						</div>
						<div className="row align-items-center" style={{ marginBottom: '5px' }}>
							<div className="col">
								<QuantrCalendar style={this.style2} />
							</div>
							<div className="col" style={{ margin: 'auto' }}>
								<h1 className="codeH1">Styling via ReactJS</h1>
								<pre className="code">
									{`private style2 = {
	prevMonth: 'red'
};
<QuantrCalendar style={this.style2} />`}
								</pre>
							</div>
						</div>
						<div className="row align-items-center" style={{ marginBottom: '5px' }}>
							<div className="col">
								<QuantrCalendar myCssClass="myCalendarClass" />
							</div>
							<div className="col" style={{ margin: 'auto' }}>
								<h1 className="codeH1">Styling via css class</h1>
								<pre className="code">
									{`<style>
.myCalendarClass .prevMonth {
	color: blue;
	background-color: pink;
	border-radius: 3px;
}

.myCalendarClass .nextMonth {
	color: yellow;
	background-color: green;
	border-radius: 3px;
}

.myCalendarClass .today {
	color: white;
	background-color: red;
	border-radius: 3px;
}
</style>
<QuantrCalendar myCssClass="myCalendarClass" />`}
								</pre>
							</div>
						</div>
						<div className="row align-items-center" style={{ marginBottom: '5px' }}>
							<div className="col-10">
								<QuantrMultipleCalendar myCssClass="myMultipleCalendarClass" multiple={[10, 11, 12]} year={2018} />
							</div>
							<div className="col-2" style={{ margin: 'auto' }}>
								<h1 className="codeH1">Styling via css class</h1>
								<pre className="code">

								</pre>
							</div>
						</div>
					</div>
					<a href="http://www.quantr.hk" target="_blank" style={{ color: 'white', textDecoration: 'none' }}>www.quantr.hk</a>
				</header>
			</div>
		);
	}
}

export default App;
