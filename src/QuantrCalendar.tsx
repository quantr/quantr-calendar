import React, { Component } from 'react';
import logo from './logo.svg';
import styles from './QuantrCalendar.module.scss';

import { Moment } from 'moment';
var moment = require('moment');
import { number } from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faGhost, faCalendar, faCalendarAlt, faUsers, faUserMd, faMale, faClock, faMoneyBill, faPenFancy, faUserClock, faPills, faBusinessTime, faPlaneDeparture } from '@fortawesome/free-solid-svg-icons';
import * as fontawesome from '@fortawesome/free-solid-svg-icons';

var classnames = require('classnames');

class QuantrCalendar extends Component<{ style?: any, myCssClass?: string, month?: number, year?: number, hideTopBar?: boolean }, {}> {
	private rows: any[] = [];
	public weekDayNames: string[] = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
	private currentDate: Moment = new moment();
	private hideTopBar: boolean = false;

	constructor(props: any) {
		super(props);

		if (this.props.hideTopBar == true) {
			this.hideTopBar = true;
		}

		if (this.props.month == null || this.props.month == null) {
			this.loadCalendar();
		} else {
			this.setMonth(this.props.year as number, this.props.month);
		}
	}

	render() {
		var currentDateStr = moment(this.currentDate).format("YYYY-MM");
		return (
			<div className={classnames(styles.QuantrCalendar, this.props.myCssClass)}>
				<div className="container">
					<div className="row" style={{ display: this.hideTopBar ? "none" : "" }}>
						<div className="col">
							<span className="align-middle" onClick={() => this.changeMonth(-1)} ><FontAwesomeIcon icon={fontawesome.faArrowLeft} size="1x" className={classnames("arrowLeft", styles.arrowLeft)} /></span>
						</div>
						<div className="col">
							<span className="align-middle">{currentDateStr}</span>
						</div>
						<div className="col">
							<span className="align-middle" onClick={() => this.changeMonth(1)} ><FontAwesomeIcon icon={fontawesome.faArrowRight} size="1x" className={classnames("arrowRight", styles.arrowRight)} /></span>
						</div>
					</div>
					{this.rows}
				</div>
			</div>
		);
	}

	private changeMonth(monthNo: number) {
		this.currentDate = new moment(this.currentDate).add(monthNo, 'months');
		this.loadCalendar();
		this.forceUpdate();
	}

	private setMonth(year: number, monthNo: number) {
		this.currentDate = new moment(year + "-" + monthNo + "-01");
		this.loadCalendar();
		this.forceUpdate();
	}

	private loadCalendar() {
		console.log('current=' + this.currentDate.format("YYYY-MM-DD"));
		let temp = moment(this.currentDate.format("YYYY-MM-01"), "YYYY-MM-DD");
		let dayOfWeek: number = temp.weekday();
		console.log('dayOfWeek=' + dayOfWeek);
		let numberOfWeek: number = (this.getWeekNums(temp));
		console.log('x=' + temp.clone().add(1, 'months').format("YYYY-MM-DD"));
		let lastDayOfMonth = temp.clone().add(1, 'months').add(-1, "days").date();
		console.log('lastDayOfMonth=' + lastDayOfMonth);
		let lastDayOfLastMonth = temp.clone().add(-1, "days").date();
		let todayDate = moment().date();

		let columns = [];
		for (var x = 0; x < 7; x++) {
			columns.push(<div className="col weekday">{this.weekDayNames[x]}</div>);
		}
		this.rows = [];
		this.rows.push(<div className="row">{columns}</div>);
		var day = 1;
		console.log('numberOfWeek=' + numberOfWeek);
		for (var y = 0; y < numberOfWeek; y++) {
			columns = [];
			console.log('current month=' + this.currentDate.month());
			console.log('styles.today');
			console.log(styles.today);
			if (y == 0) {
				//first week
				for (var x = 0; x < dayOfWeek; x++) {
					// last month
					columns.push(<div className={classnames("col", "prevMonth", styles.prevMonth)} style={{ color: this.getCustomStyle('prevMonth') }}> {lastDayOfLastMonth - dayOfWeek + x + 1}</div >);
				}
				for (var x = dayOfWeek; x < 7; x++) {
					if (todayDate == day && this.currentDate.month() + 1 == new moment().month() + 1 && this.currentDate.year() == new moment().year()) {
						columns.push(<div className={classnames("col day", "today", styles.today)} style={{ color: this.getCustomStyle('today'), backgroundColor: this.getCustomStyle('todayBackground') }}>{day}</div>);
					} else {
						columns.push(<div className="col day">{day}</div>);
					}
					day++;
				}
			} else {
				this.rows.push(<br />);
				for (var x = 0; x < 7; x++) {
					if (day <= lastDayOfMonth) {
						if (todayDate == day && this.currentDate.month() + 1 == new moment().month() + 1 && this.currentDate.year() == new moment().year()) {
							columns.push(<div className={classnames("col day", "today", styles.today)} style={{ color: this.getCustomStyle('today'), backgroundColor: this.getCustomStyle('todayBackground') }}>{day}</div>);
						} else {
							columns.push(<div className="col day">{day}</div>);
						}
						day++;
					} else {
						// next month
						columns.push(<div className={classnames("col", "nextMonth", styles.nextMonth)} style={{ color: this.getCustomStyle('nextMonth') }}>{day - lastDayOfMonth}</div>);
						day++;
					}
				}
			}
			this.rows.push(<div className="row flex-nowrap">{columns}</div>);
		}
	}

	private getWeekNums(momentObj: Moment) {
		var clonedMoment = moment(momentObj), first, last;

		// get week number for first day of month
		first = clonedMoment.startOf('month').week();
		// get week number for last day of month
		last = clonedMoment.endOf('month').week();
		if (last < 4) {
			last += 4;
		}

		// In case last week is in next year
		if (first > last) {
			last = first + last;
		}
		return last - first + 1;
	}

	private getCustomStyle(p: any) {
		if (this.props.style == null) {
			return null;
		}
		return this.props.style[p];
	}
}

export default QuantrCalendar;