This calendar is built by ReactJS+TypeScript+SCSS, you can use it in a ReactJS project or SharePoint SPfx project.

# Usage

```
npm i --save @quantr/quantr-calendar node-sass
```

```
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import QuantrCalendar from '@quantr/quantr-calendar';

class App extends Component {
	render() {
		return (
			<div>
				<QuantrCalendar />
			</div>
		);
	}
}

export default App;
```

# webpack issue

DO NOT add webpack and webpack-cli to package.json, either <dependencies> or <devDependencies>, manually run "npm i -g webpack webpack-cli"
