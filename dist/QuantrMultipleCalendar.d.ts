import { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
declare class QuantrMultipleCalendar extends Component<{
    style?: any;
    myCssClass?: string;
    multiple: number[];
    year: number;
}, {}> {
    private currentDateStr;
    constructor(props: any);
    render(): JSX.Element;
}
export default QuantrMultipleCalendar;
